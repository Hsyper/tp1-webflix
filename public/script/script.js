"use strict";

jQuery(document).ready(function ($) {

    $("#icon-hamburger-mobile").click(function () {
        $("#menu-mobile").removeClass("show-on-mobile");
        $("#menu-mobile").toggleClass("hide");
    });

    $(".close, .lien-detail").click(function () {
        $("#details-nouveautes").toggleClass("hide");
    });

    $(".carousel").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
    });

    //$("#onglets").tabs();
    $(".tab").click(function () {
        $(".tab").removeClass("current");
        $(this).addClass("current");
        $("#tabs-1, #tabs-2, #tabs-3").addClass("hide");
        let tabId = $(this).attr("href");
        $(tabId).removeClass("hide")
    });

    var tags = ["lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", 
        "proin", "faucibus", "elementum", "malesuada", "eros", "rutrum", "ultricies", "aenean", 
        "aliquam", "gravida", "sapien", "vel", "praesent", "quis", "mi", "et", 
        "elit", "tempor", "cras", "tristique", "venenatis", "tortor", "id", "nunc", "sem", 
        "suscipit", "eget", "volutpat", "at", "sagittis", "in", "sed", "interdum", 
        "augue", "ornare", "auctor", "enim", "maecenas", "commodo", "orci", "pretium", 
        "viverra", "fermentum", "nibh", "arcu", "sapien", "eu", "dui", "curabitur"];

    $("#recherche").autocomplete({
        max: 10,
        source: tags
    })

});
