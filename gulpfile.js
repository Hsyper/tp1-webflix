var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefix  = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var rename = require('gulp-rename');

gulp.task('watch', function(){
    browserSync.init({
        server: {
            baseDir: './public'
        }
    })
    gulp.watch('scss/**/*.scss', gulp.series('sass')); 
    gulp.watch('public/**/*.html').on('change', browserSync.reload);
    gulp.watch('public/**/*.js').on('change', browserSync.reload);
})

gulp.task('sass', function(){
    return gulp.src('scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .on('error', function(err){
            browserSync.notify(err.message, 15000);
            this.emit('end');
        })
        .pipe(autoprefix())
        .pipe(sourcemaps.write('./'))
        /*.pipe(rename({ extname: '.min.css' }))*/
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.reload({stream: true}));
});




